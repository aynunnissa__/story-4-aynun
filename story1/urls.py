from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='story1-index'),
]