$(function () { 
    var sideBar = $(".side-nav");
    $(window).scroll(function () { 
        var scroll = $(window).scrollTop(); 
        if (scroll >= window.innerHeight) { 
            sideBar.css("opacity","1");
            sideBar.css("z-index","0");
        } else { 
            sideBar.css("opacity","0");
            sideBar.css("z-index","-1"); 
        } 
    }); 
}); 