from django.urls import path

from . import views
# from story4.views import Homepage, Project, Interest

urlpatterns = [
	path('', views.homepage, name='index'),
    path('project/', views.project, name='project'),
    path('interests/', views.interest, name='interest'),
    # path('', Homepage.as_view(), name='index'),
    # path('project/', Project.as_view(), name='project'),
    # path('interests/', Interest.as_view(), name='interest'),
]