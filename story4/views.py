from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

def homepage(request) :
	return render(request, 'story4/index.html', {'homepage' : 1})

def project(request) :
	return render(request, 'story4/project.html', {'project' : 1})

def interest(request) :
	return render(request, 'story4/interests.html', {'interest' : 1})

# class Homepage(TemplateView):
#     template_name = "story4/index.html"

# class Project(TemplateView):
#     template_name = "story4/project.html"

# class Interest(TemplateView):
#     template_name = "story4/interests.html"

